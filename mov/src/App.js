import React from 'react';
import './App.css';
import { getmovies } from "./API"
import { getmoremovies } from "./API"
//import { gettvshows } from "./API"
import Cardlist from './components/cards/cards';
import Pages from './components/pagere/pages';


class App extends React.Component {
  state = {
    fdata: [],
    name: [],
    poster: [],
    pos: '',
    lang: [],
    date: [],
    overv: [],
    adult: [],
    id: [],
    show: false,
    whole: {}
  }
  /* async componentDidMount() {
     const API_KEY = '76185723bda30688044647742006134b';
     const url = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
     const data = await fetch(url);
     const res = await data.json()
     console.log(res);
   }*/

  async componentDidMount() {
    const fdata = await getmovies();
    this.setState({ fdata: fdata })
    var i
    for (i = 0; i < fdata.length; i++) {
      this.state.name.push(fdata[i].original_title);
      this.setState({ name: this.state.name })
    }
    for (i = 0; i < fdata.length; i++) {
      this.setState({ pos: fdata[i].backdrop_path })
      let sam = this.state.pos.toString()
      // let nsam = sam.replace(".jpg", "_movie_poster.jpg")
      this.state.poster.push(`http://image.tmdb.org/t/p/w342${sam}`);
      this.setState({ poster: this.state.poster })
    }
    for (i = 0; i < fdata.length; i++) {
      this.state.lang.push(fdata[i].original_language);
      this.setState({ lang: this.state.lang })
      this.state.date.push(fdata[i].release_date);
      this.setState({ date: this.state.date })
      this.state.overv.push(fdata[i].overview);
      this.setState({ overv: this.state.overv })
      this.state.adult.push(fdata[i].adult);
      this.setState({ adult: this.state.adult })
      this.state.id.push(fdata[i].id);
      this.setState({ id: this.state.id })
    }
    console.log(this.state.adult)


  }
  handler = async (page) => {
    const fdata = await getmoremovies(page);
    this.setState({ fdata: fdata })
    this.state.name = []
    this.state.poster = []
    this.state.lang = []
    this.state.date = []
    this.state.overv = []
    this.state.adult = []
    this.state.id = []
    var i
    for (i = 0; i < fdata.length; i++) {
      this.state.name.push(fdata[i].original_title);
      this.setState({ name: this.state.name })
    }
    for (i = 0; i < fdata.length; i++) {
      this.setState({ pos: fdata[i].backdrop_path })
      let sam = this.state.pos.toString()
      // let nsam = sam.replace(".jpg", "_movie_poster.jpg")
      this.state.poster.push(`http://image.tmdb.org/t/p/w342${sam}`);
      this.setState({ poster: this.state.poster })
    }
    for (i = 0; i < fdata.length; i++) {
      this.state.lang.push(fdata[i].original_language);
      this.setState({ lang: this.state.lang })
      this.state.date.push(fdata[i].release_date);
      this.setState({ date: this.state.date })
      this.state.overv.push(fdata[i].overview);
      this.setState({ overv: this.state.overv })
      this.state.adult.push(fdata[i].adult);
      this.setState({ adult: this.state.adult })
      this.state.id.push(fdata[i].id);
      this.setState({ id: this.state.id })
    }
  }


  render() {
    var names = this.state.name
    var posters = this.state.poster
    var dates = this.state.date
    var langs = this.state.lang
    var adults = this.state.adult
    var overvs = this.state.overv
    var ids = this.state.id
    const cardlists = names.map((cardlists, items) => {
      return (
        <>
          <Cardlist name={cardlists} log={posters[items]} da={dates[items]} la={langs[items]} id={ids[items]} overview={overvs[items]} more={adults[items]} />
        </>
      )

    })

    return (
      <div className="mainpage">
        <Pages handler={this.handler} />
        <div className="container" >
          <div className="row" >
            {cardlists}
          </div>
        </div>

      </div>



      /*</a><div style={{ 'white-space': 'pre-line' }}>
        {this.state.name.map((j) => j).join('\n')}
        <br />
       // {this.state.poster.map((i) => <img src={i} alt=" " />)}
    </div> -->*/


    )
  }
}

export default App;
