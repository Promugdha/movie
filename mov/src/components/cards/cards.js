import React from "react";


const Cardlist = (props) => {
    if (props.more === false) {
        var play = 'NO'
    }
    else {
        play = 'YES'
    }
    return (


        <div className="col-12 col-md-4">

            <div className="card mt-5">

                <h3 className="card-header bg-success text-black">
                    {props.name}
                </h3>

                <div className="card-body">
                    <img src={props.log} alt=" " width="310" height="192" />
                    <h1>Release Date:{props.da}</h1>
                    <h2>Language:{props.la}</h2>
                    <div id="accordion">
                        <div className="card">
                            <div className="card-header" role="tab" id="aghe">
                                <a data-toggle="collapse" data-target={`#ag${props.id}`} href=" ">
                                    <h3>MORE <small>......</small>
                                    </h3>
                                </a>
                            </div>
                            <div className="collapse" id={`ag${props.id}`} data-parent="#accordion">
                                <div className="card-body">

                                    <p >{props.overview}</p>

                                    <h5>Adult:{play} </h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
}

export default Cardlist;