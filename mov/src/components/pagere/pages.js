import React from 'react';
import { FormControl, NativeSelect } from "@material-ui/core";
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
const Pages = (props) => {
    return (
        <FormControl className="btn btn-success position-fixed" >
            <NativeSelect className="bg-success btn btn-success" defaultValue=" " onChange={(e) => props.handler(e.target.value)}>
                <option value="">PAGES</option>
                {arr.map((ca) => (
                    <option className="bg-danger text-white" key={ca} value={ca}>{ca}</option>
                ))}
            </NativeSelect>
        </FormControl>
    )
}

export default Pages;