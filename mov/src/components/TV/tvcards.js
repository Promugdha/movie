import React from 'react';
import { Card, CardImg, CardImgOverlay, CardText, CardBody, CardTitle } from 'reactstrap';

const TVcard = (props) => {

    return (
        <>
            {props.result.map((items) =>

                <div key={items.id} className="col-12 col-md-4 mt-5 ">
                    <Card>
                        <CardImg width="100%" object src={`http://image.tmdb.org/t/p/w342${items.backdrop_path}`} alt={items.id} />
                        <CardImgOverlay>
                            <CardTitle heading className="text-white "><h5>{items.name}</h5></CardTitle>

                        </CardImgOverlay>
                        <CardBody>
                            <CardTitle>Release date:{items.first_air_date}</CardTitle>
                            <CardText><h4>Language:{items.original_language}</h4> </CardText>

                            <div id="accordion">
                                <div className="card">
                                    <div className="card-header" role="tab" id="aghe">
                                        <a data-toggle="collapse" data-target={`#ag${items.id}`} href=" ">
                                            <h3>MORE <small>......</small>
                                            </h3>
                                        </a>
                                    </div>
                                    <div className="collapse" id={`ag${items.id}`} data-parent="#accordion">
                                        <div className="card-body">

                                            <p >{items.overview}</p>

                                            <h5>Origin:{items.origin_country}</h5>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </CardBody>
                    </Card>
                </div>
            )}
        </>
    )
}

export default TVcard;