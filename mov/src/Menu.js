import React from 'react';
import { Link } from 'react-router-dom';
import './Menu.css'

const Menu = () => {
    return (
        <div className="Menustyle">
            <ul>
                <li><Link to="/">MOVIES</Link></li>
                <li><Link to="tv">TV-SHOWS</Link></li>
            </ul>
        </div>
    )
}
export default Menu;