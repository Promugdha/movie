import Axios from 'axios';
const API_KEY = '76185723bda30688044647742006134b';
export const getmovies = async () => {
    const url = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
    const data1 = await Axios.get(url)
    return data1.data.results
}
export const getmoremovies = async (page) => {
    let url = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=${page}`;
    let data1 = await Axios.get(url)
    return data1.data.results
}
//TV SHOWS
export const gettvshows = async (page) => {
    const url = `https://api.themoviedb.org/3/tv/popular?api_key=${API_KEY}&language=en-US&page=${page}`;
    const data2 = await Axios.get(url, {
        params: {
            page: page,
        }
    })
    console.log(data2.data)
    return data2.data.results;
}


