import React, { Component } from 'react';
import { gettvshows } from "./API";
import './TVApp.css';
import TVcard from "./components/TV/tvcards";

class Tvshows extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tvshows: [],
            page: 1,
            boo: false,
            boo1: false
        }
    }
    async componentDidMount() {
        let fdata = await gettvshows(this.state.page);
        this.setState({ tvshows: fdata })
        this.setState({ boo: true })
        console.log(this.state.tvshows)
    }
    pager() {
        if (this.state.page === 500) {
            this.setState({ boo1: true })
        }
        else if (this.state.page === 2) {
            this.setState({ boo: true })
        }
        else {
            this.setState({ boo: false, boo1: false })
        }
    }
    async renderpage() {

        this.setState({ page: this.state.page + 1 })
        var fdata = await gettvshows(this.state.page + 1);
        this.setState({ tvshows: fdata })

        console.log(this.state.tvshows)

    }
    async renderpageback() {

        this.setState({ page: this.state.page - 1 })
        var fdata = await gettvshows(this.state.page - 1);
        this.setState({ tvshows: fdata })
        console.log(this.state.tvshows)
    }


    render() {

        return (
            <div className="con">
                <div className="container">
                    <div className="row">
                        <TVcard result={this.state.tvshows} />

                        <div className="col-12 col-md-3  offset-md-9 mb-5">
                            <button className=" btn btn-success btn-block text-black " onClick={() => { this.renderpage(); this.pager(); }} disabled={this.state.boo1} > NEXT PAGE: {this.state.page} </button>
                            <button className=" btn btn-danger btn-block text-black" onClick={() => { this.renderpageback(); this.pager(); }} disabled={this.state.boo}> PREVIOUS PAGE: {this.state.page} </button>
                        </div>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
        )
    }

}

export default Tvshows;