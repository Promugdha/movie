import React from 'react';
import App from './App';
import Tvshows from './TVApp';
import Menu from './Menu';
import { BrowserRouter, Route } from 'react-router-dom';

const Compiler = () => {
    return (
        <BrowserRouter>
            <div>
                <Menu />
                <Route path="/" exact component={App} />
                <Route path="/tv" component={Tvshows} />
            </div>
        </BrowserRouter>
    )
}
export default Compiler;
